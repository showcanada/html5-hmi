﻿
var vertexShaderText =
[
'precision mediump float;',
'',
'attribute vec3 vertPosition;',
'attribute vec2 vertTextCoord;',
'varying vec2 fragTextCoord;',
'uniform mat4 mWorld;',
'uniform mat4 mView;',
'uniform mat4 mProj;',
'uniform vec4 translation;',
'',
'void main()',
'{',
' fragTextCoord = vertTextCoord;',
' gl_Position = mProj * mView *  mWorld * vec4(vertPosition, 1.0);',
'}'
    ].join('\n');


var fragmentSharderText =
[
'precision mediump float;',
'',
'varying vec2 fragTextCoord;',
'uniform sampler2D sampler;',
'',
'void main()',
'{',
'gl_FragColor = texture2D(sampler, fragTextCoord);',
'}'
    ].join('\n');







function iniGL() {
    console.log('This is working!.')

    var objectcanvas = document.getElementById("gl");

    
  
    var gl = objectcanvas.getContext("experimental-webgl") ||
        objectcanvas.getContext("webgl")||
     objectcanvas.getContext("webkit-3d");

    if (!gl) {
        console.log('WEbGL not supported');
    }


    gl.clearColor(0.75, 0.85, 0.8, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.enable(gl.DEPTH_TEST);
    gl.enable(gl.CULL_FACE);
    gl.frontFace(gl.CCW);
    gl.cullFace(gl.BACK);

    var vertexShader = gl.createShader(gl.VERTEX_SHADER);
    var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(vertexShader, vertexShaderText);
    gl.shaderSource(fragmentShader, fragmentSharderText);

    gl.compileShader(vertexShader);

    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
        console.error('Error compiling vertex Shader', gl.getShaderInfoLog(vertexShader));
        return;
    }

    gl.compileShader(fragmentShader);

    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {

        console.error('Error compiling fragment Shader', gl.getShaderInfoLog(fragmentShader));
        return;
    }

    var program = gl.createProgram();

    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);

    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
        console.error('ERROR linking program!', gl.getProgramInfoLog(program));
        return;
    }



    gl.validateProgram(program);

    if (!gl.getProgramParameter(program, gl.VALIDATE_STATUS)) {
        console.error('ERROR validating program!', gl.getProgramInfoLog(program));
        return;
            }
   

    //
    // Create buffer
    //

    var boxVerticeBig =
        [ //X,Y , Z          U,V
            // Top
		-1.0, 1.0, -1.0,     0, 0,
		-1.0, 1.0, 1.0,      0, 1,
		1.0, 1.0, 1.0,       1, 1,
		1.0, 1.0, -1.0,      1, 0,

		// Left
		-1.0, 1.0, 1.0,      0, 0,
		-1.0, -1.0, 1.0,     1, 0,
		-1.0, -1.0, -1.0,    1, 1,
		-1.0, 1.0, -1.0,     0, 1,

		// Right
		1.0, 1.0, 1.0,       1, 1,
		1.0, -1.0, 1.0,      0, 1,
		1.0, -1.0, -1.0,     0, 0,
		1.0, 1.0, -1.0,      1, 0,

		// Front
		1.0, 1.0, 1.0,       1, 1,
		1.0, -1.0, 1.0,      1, 0,
		-1.0, -1.0, 1.0,     0, 0,
		-1.0, 1.0, 1.0,      0, 1,

		// Back
		1.0, 1.0, -1.0,      0, 0,
		1.0, -1.0, -1.0,     0, 1,
		-1.0, -1.0, -1.0,    1, 1,
		-1.0, 1.0, -1.0,     1, 0,

		// Bottom
		-1.0, -1.0, -1.0,    1, 1,
		-1.0, -1.0, 1.0,     1, 0,
		1.0, -1.0, 1.0,      0, 0,
		1.0, -1.0, -1.0,     0, 1,
        ];


    var boxVertices =
     [ //X,Y , Z          U,V
         // Top
     -0.5, 0.5, -0.5,    0, 0,
     -0.5, 0.5, 0.5,     0, 1,
     0.5, 0.5, 0.5,     1, 1,
     0.5, 0.5, -0.5,     1, 0,

     // Left
     -0.5, 0.5, 0.5,    0, 0,
     -0.5, -0.5, 0.5,   1, 0,
     -0.5, -0.5, -0.5,  1, 1,
     -0.5, 0.5, -0.5,   0, 1,

     // Right
     0.5, 0.5, 0.5,     1, 1,
     0.5, -0.5, 0.5,    0, 1,
     0.5, -0.5, -0.5,   0, 0,
     0.5, 0.5, -0.5,    1, 0,

     // Front
     0.5, 0.5, 0.5,     1, 1,
     0.5, -0.5, 0.5,    1, 0,
     -0.5, -0.5, 0.5,   0, 0,
     -0.5, 0.5, 0.5,    0, 1,

     // Back
     0.5, 0.5, -0.5,    0, 0,
     0.5, -0.5, -0.5,   0, 1,
     -0.5, -0.5, -0.5,  1, 1,
     -0.5, 0.5, -0.5,   1, 0,

     // Bottom
     -0.5, -0.5, -0.5,  1, 1,
     -0.5, -0.5, 0.5,   1, 0,
     0.5, -0.5, 0.5,    0, 0,
     0.5, -0.5, -0.5,   0, 1,
     ];

    var boxIndices =
	[
		// Top
		0, 1, 2,
		0, 2, 3,

		// Left
		5, 4, 6,
		6, 4, 7,

		// Right
		8, 9, 10,
		8, 10, 11,

		// Front
		13, 12, 14,
		15, 14, 12,

		// Back
		16, 17, 18,
		16, 18, 19,

		// Bottom
		21, 20, 22,
		22, 20, 23
	];


    var boxIndicesBig =
	[
		// Top
		0, 1, 2,
		0, 2, 3,

		// Left
		5, 4, 6,
		6, 4, 7,

		// Right
		8, 9, 10,
		8, 10, 11,

		// Front
		13, 12, 14,
		15, 14, 12,

		// Back
		16, 17, 18,
		16, 18, 19,

		// Bottom
		21, 20, 22,
		22, 20, 23
	];

    var boxVertExtBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, boxVertExtBufferObject);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(boxVertices), gl.STATIC_DRAW);

    var box2VertExtBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, box2VertExtBufferObject);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(boxVertices), gl.STATIC_DRAW);



    var boxIndexBufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, boxIndexBufferObject);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(boxIndices), gl.STATIC_DRAW);

    var positionAttribLocation = gl.getAttribLocation(program, 'vertPosition');
    var textCoordAttribLocation = gl.getAttribLocation(program, 'vertTextCoord');

    gl.vertexAttribPointer(
        positionAttribLocation, // Attribute location
        3, // Number of element per atrribute
        gl.FLOAT, // Type of element
        gl.FALSE,
        5 * Float32Array.BYTES_PER_ELEMENT, //Size of individual vertex
        0);// offset from the beginin);

    gl.vertexAttribPointer(
       textCoordAttribLocation, // Attribute location
       2, // Number of element per atrribute
       gl.FLOAT, // Type of element
       gl.FALSE,
       5 * Float32Array.BYTES_PER_ELEMENT, //Size of individual vertex
       3 * Float32Array.BYTES_PER_ELEMENT);// offset from the beginin);

    gl.enableVertexAttribArray(positionAttribLocation);
    gl.enableVertexAttribArray(textCoordAttribLocation);


    //
    // Create Texture
    //
    var boxtexture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, boxtexture);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    
    gl.texImage2D(
        gl.TEXTURE_2D, 0, gl.RGBA , gl.RGBA,
        gl.UNSIGNED_BYTE, document.getElementById('CrateImg'));


    gl.bindTexture(gl.TEXTURE_2D, null);


    // Tell OpenGL state machie which program should be active
    gl.useProgram(program);


    var matWorldUniformLocation = gl.getUniformLocation(program, 'mWorld');
    var matViewUniformLocation = gl.getUniformLocation(program, 'mView');
    var matProjUniformLocation = gl.getUniformLocation(program, 'mProj');


    var worldMatrix = new Float32Array(16);
    var viewMatrix = new Float32Array(16);
    var projMAtrix = new Float32Array(16);

    mat4.identity(worldMatrix);
    mat4.lookAt(viewMatrix, [15, 3, 20], [0, 4, 0], [0, 2, 0]);

    mat4.perspective(projMAtrix, glMatrix.toRadian(45), objectcanvas.width / objectcanvas.height, 0.1, 1000.0);
    

    gl.uniformMatrix4fv(matWorldUniformLocation, gl.FALSE, worldMatrix);
    gl.uniformMatrix4fv(matViewUniformLocation, gl.FALSE, viewMatrix);
    gl.uniformMatrix4fv(matProjUniformLocation, gl.FALSE, projMAtrix);

  

    //
    // Main redner loop
    //
    //
    var identityMatrix = new Float32Array(16);
    var mvMatrix = mat4.create();

    mat4.identity(identityMatrix);
    var angle = 0;
    var mvMatrixStack = [];

 


    function mvPushMatrix() {
        var copy = mat4.create();
        mat4.set(mvMatrix, copy);
        mvMatrixStack.push(copy);
    }


    var loop = function () {
        angle = performance.now() / 1800 / 6 * 2 * Math.PI;
        mat4.rotate(worldMatrix, identityMatrix, angle, [0, 2, 0]);

      

        gl.uniformMatrix4fv(matWorldUniformLocation, gl.FALSE, worldMatrix);

   


        gl.clearColor(0.75, 0.85, 0.8, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        gl.bindTexture(gl.TEXTURE_2D, boxtexture);
        gl.activeTexture(gl.TEXTURE0);

        gl.drawElements(gl.TRIANGLES, boxIndices.length, gl.UNSIGNED_SHORT, 0);
       

       // angle = angle / 2;

       
       





        requestAnimationFrame(loop);

        var translation = vec3.create();

        vec3.set(translation, 0, 0, -15.0);

        mat4.translate(worldMatrix, worldMatrix, translation);


        gl.clearColor(0.75, 0.85, 0.8, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        gl.bindTexture(gl.TEXTURE_2D, boxtexture);
        gl.activeTexture(gl.TEXTURE0);

        gl.uniformMatrix4fv(matWorldUniformLocation, gl.FALSE, worldMatrix);
        gl.drawElements(gl.TRIANGLES, boxIndicesBig.length, gl.UNSIGNED_SHORT, 0);


    };
    requestAnimationFrame(loop);


  

}

