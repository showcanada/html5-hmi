﻿

// ********************************************************************//
// This function create the vertical progress bar from the HMIobject 
// and parent div ID.
// ********************************************************************//
function createHorizontalProgressBar(parentID) {

    // Create the actual display.
    //if (!Obj.type == typeof (HMIProgress)) {
    //    return;

    //}

    var parentDiv = document.getElementById(parentID);

    //
    var popupDiv = document.createElement('div');

    popupDiv.className = 'popup';

    var newspan = document.createElement('span');

    newspan.className = 'popuptext';

    newspan.id = parentID + '_HNewSpan';

    popupDiv.appendChild(newspan);



    parentDiv.appendChild(popupDiv);


    //
    var newVmeter = document.createElement('div');

    newVmeter.className = 'meter animate';

    var newVProgress = document.createElement('div');

    newVProgress.id = parentID + '_HProgress';

    var newlabel = document.createElement('Div');

    newlabel.className = 'progresslabel';

    newlabel.id = parentID + '_HProgressLabel';

    newVProgress.appendChild(newlabel);


    newVmeter.appendChild(newVProgress);


    parentDiv.appendChild(newVmeter);


}


// ********************************************************************//
// This function evaluate the plc tag value and evaluate if it reach
// warning, critical level and change the display in the HTML view.
//
// ********************************************************************//
function evaluateHorizontalProgress(Obj, value) {

    var newHProgress = document.getElementById(Obj.parentID + '_HProgress');
    newHProgress.className = "meter";


    var newspan = document.getElementById(Obj.parentID + '_HNewSpan');

    var newlabel = document.getElementById(Obj.parentID + '_HProgressLabel');

    // Evaluation of treshold critical level
    var warnPercentage = Obj.warninglevel / Obj.maxValue * 100;
    var warnLevel = parseInt(warnPercentage);

    var critPercentage = Obj.criticalLevel / Obj.maxValue * 100;
    var critLevel = parseInt(critPercentage);


    //  TODO change if the value is null (unknow state)
    if (Obj.tag.value == null) {

        var parentElement = document.getElementById(Obj.parentID);



    }

    // Implement the Logic

    var percentage = Obj.tag.value / Obj.maxValue * 100;

    var actuelWidth = parseInt(percentage);

    if (actuelWidth > warnLevel) {

        newHProgress.className = "";
        newHProgress.className = "meter orange";
    }

    if (actuelWidth > critLevel) {
        newHProgress.className = "";

        newHProgress.className = "meter red";


    }

    if (actuelWidth < warnLevel) {
        newHProgress.className = "";

        newHProgress.className = "vmeter";
    }

    var width = actuelWidth;

    newHProgress.style.width = width + '%';;




    newlabel.innerHTML = Obj.tag.value;


}

// ********************************************************************//
// This will create the vertical progress bar . 
// This user need to implement the Div ID in order to create the widget.
// ********************************************************************//
function HorizontalProgress(parentID) {

  
    this.setPriority = function (value) {

        this.priority = value;
        if (this.tag.plc.AMSAddress.length > 0 && value.length > 0 && this.tag.type.length > 0
            && this.priority !== undefined) {
            this.bind();
        }

    }


    this.connectTag = function (plcObj, value, type) {
        this.tag = new TagObject(plcObj, "", type, this, evaluateHorizontalProgress);
        this.tag.tagName = value;

        if (this.tag.plc.AMSAddress.length > 0 && value.length > 0 && this.tag.type.length > 0
           && this.priority !== undefined) {
            this.bind();
        }

    }
    this.HorizontalProgressElement = createHorizontalProgressBar(parentID);

   

    this.setMaxValue = function (value) {
        this.maxValue = value;

        if (!this.tag.value == null) {
            evaluateVertProgress(this);
        }

    }


    this.setValueType = function (value) {
        this.tag.type = value
        if (this.tag.plc.AMSAddress.length > 0 && this.tag.tagName.length > 0 && !value == undefined
            && this.priority !== undefined) {
            this.bind();
        }

    }



    this.bind = function () {
        if (this.priority === "medium") {
            mediumPollingTagArray.push(this.tag);
        }

        if (this.priority === "fast") {
            fastPollingTagArray.push(this.tag);
        }

        if (this.priority === "slow") {
            slowPollingTagArray.push(this.tag);
        }

        //default polling
        if (this.priority == undefined) {
            mediumPollingTagArray.push(this.tag);

        }
    }


    this.setMinValue = function (value) {
        this.minValue = value;
        if (!this.tag.value == null) {
            evaluateVertProgress(this);
        }

    }


 

    this.setwarninglevel = function (value) {
        this.warninglevel = value;
        if (!this.tag.value == null) {
            evaluateVertProgress(this);
        }

    }



    this.setcriticalLevel = function (value) {
        this.criticalLevel = value;

        if (!this.tag.value == null) {
            evaluateVertProgress(this);
        }


    }


    this.parentID = parentID;

}

