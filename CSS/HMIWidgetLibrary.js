﻿
//*****************************************************//
//*****************************************************//
// Global objects




// ********************************************************************//
// ********************************************************************//
// ********************************************************************//
// Table Logic
// ********************************************************************//

//Call to create dynamic table from array.
function tableCreate(array) {
    var table = document.getElementById("myTable");


    //Get the count of columns.
    var columncount = array[0].length;


    //Add the header row.
    var row = table.insertRow(-1);

    for (var i = 0; i < columncount; i++) {
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = array[0][i].toString();
        row.appendChild(headerCell);

    }

    //Add the data rows.
    for (var i = 1; i < fakeData.length; i++) {
        row = table.insertRow(-1);
        for (var j = 0; j < columncount; j++) {

            var cell = row.insertCell(-1);
            cell.innerHTML = array[i][j];
        }

    }
}



// ********************************************************************//
// ********************************************************************//
// ********************************************************************//
// Button Logic
// ********************************************************************//

//Call to create button.
function createwidgetButton(ValueText, SelfID, plcTag, priorityClass, parentId) {

    var button = document.createElement("input");

    var buttonClass = button.classList;

    button.type = "submit";

    button.value = ValueText;

    buttonClass.add("btn");

    document.getElementById(parentId).appendChild(button);


}

// ********************************************************************//
// ********************************************************************//
// ********************************************************************//
//Date function()
function datetime() {
    date = new Date;
    year = date.getFullYear();
    month = date.getMonth();
    months = new Array('January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December');
    d = date.getDate();
    day = date.getDay();
    days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday',
        'Thursday', 'Friday', 'Saturday');
    h = date.getHours();
    if (h < 10) {
        h = "0" + h;
    }
    m = date.getMinutes();
    if (m < 10) {
        m = "0" + m;
    }
    s = date.getSeconds();
    if (s < 10) {
        s = "0" + s;
    }
    result = '' + days[day] + ' ' + months[month] + ' ' + d + ' '
        + year + ' ' + h + ':' + m + ':' + s;
    document.getElementById("datetime").innerHTML = result;
    setTimeout('datetime("' + "datetime" + '");', '1000');
    return true;
}



// ********************************************************************//
// ********************************************************************//
// ********************************************************************//
//  Tab Content Logic
// ********************************************************************//
//  This function is binded to a click event to open a the targeted tab 
//  content. 
// ********************************************************************//

//menu open tab function()
function openNewTab(evt, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the 
    // link that opened the tab
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
};

// ********************************************************************//
// ********************************************************************//
// ********************************************************************//
function LoadingPage() {

    // when the all data from all PLC has been loaded
    // correctly remove the loader from the page
    IsalldataLoaded = setTimeout(showlogin, 900);

    datetime();
}

function showPage() {
    document.getElementById("body").style.display = "block";

}

// ********************************************************************//
// ********************************************************************//
// ********************************************************************//

// ********************************************************************//
//  This login function. 
//  TODO: May not be required.  
// 
// ********************************************************************//

function showlogin() {
    var pass = "pass"
    var use = "user"

    //remove loader image
    document.getElementById("loader").style.display = "none";


    //show the login panel
    var loginPanel = document.getElementById('LoginID');
    loginPanel.style.display = "block";

    //Submit button logic
    var loginSubmit = document.getElementsByClassName('login-submit')[0];

    var exitbutton = document.getElementsByClassName("loginclose")[0];


    // Simple Logic Need to bind a password evaluation logic
    loginSubmit.onclick = function () {
        loginPanel.style.display = "none";

        showPage();
    }

    // Simple Logic Need to bind a password evaluation logic
    exitbutton.onclick = function () {
        loginPanel.style.display = "none";


        showPage();
    }

}

// ********************************************************************//
// This function create the login widget. Granted that the a div dom 
// element is previously created with
// ********************************************************************//
function createLoginWidget() {
    var loginID = document.getElementById('LoginID');

    loginID.classList.add('loginDefault');

    var newDiv = document.createElement("Div");

    newDiv.classList.add('login-card');

    newDiv.id = "login-card";

    loginID.appendChild(newDiv);


    var newspan = document.createElement("span");

    newspan.classList.add('loginclose');

    var newTextNodeX = document.createTextNode("x");

    newspan.appendChild(newTextNodeX);

    newDiv.appendChild(newspan);

    var newForm = document.createElement("Form");

    newForm.ID = "newForm";

    newDiv.appendChild(newForm);


    var newTextStyle = document.createElement("H1");

    newForm.appendChild(newTextStyle);

    var newTextNode = document.createTextNode("Login");

    newTextStyle.appendChild(newTextNode);


    newForm.appendChild(newTextStyle);


    var newTextUsr = document.createElement("input");

    newTextUsr.type = "text";
    newTextUsr.placeholder = "username";
    newTextUsr.required = "";
    newTextUsr.Id = "username";

    newForm.appendChild(newTextUsr);


    var newTextPss = document.createElement("input");

    newTextPss.type = "password";
    newTextPss.placeholder = "password";
    newTextPss.required = "";
    newTextPss.Id = "password";

    newForm.appendChild(newTextPss);


    var newSubmit = document.createElement("input");

    newSubmit.type = "submit";
    newSubmit.classList.add('login-submit');
    newSubmit.required = "";
    newSubmit.Id = "LoginSubmit";
    newSubmit.value = "Login"
    newForm.appendChild(newSubmit);

}


// ********************************************************************//
// ********************************************************************//
// ********************************************************************//




//// ********************************************************************//
//// This function create the vertical progress bar from the HMIobject 
//// and parent div ID.
//// ********************************************************************//
//function createVerticalProgressBar2(parentID) {

//    // Create the actual display.
//    //if (!Obj.type == typeof (HMIProgress)) {
//    //    return;

//    //}

//    var parentDiv = document.getElementById(parentID);

//    //
//    var popupDiv = document.createElement('div');

//    popupDiv.className = 'popup';

//    var newspan = document.createElement('span');

//    newspan.className = 'popuptext';

//    newspan.id = parentID + '_VNewSpan';

//    popupDiv.appendChild(newspan);



//    parentDiv.appendChild(popupDiv);


//    //
//    var newVmeter = document.createElement('div');

//    newVmeter.className = 'vmeter animate';

//    var newVProgress = document.createElement('div');

//    newVProgress.id = parentID + '_VProgress';

//    var newlabel = document.createElement('Div');

//    newlabel.className = 'vprogresslabel';

//    newlabel.id = parentID + '_VProgressLabel';

//    newVProgress.appendChild(newlabel);


//    newVmeter.appendChild(newVProgress);


//    parentDiv.appendChild(newVmeter);


//}


//// ********************************************************************//
//// This function evaluate the plc tag value and evaluate if it reach
//// warning, critical level and change the display in the HTML view.
////
//// ********************************************************************//
//function evaluateVertProgress(Obj ,value) {

//    var newVProgress = document.getElementById(Obj.parentID + '_VProgress');
//    newVProgress.className = "vmeter";


//    var newspan = document.getElementById(Obj.parentID + '_VNewSpan');

//    var newlabel = document.getElementById(Obj.parentID + '_VProgressLabel');

//    // Evaluation of treshold critical level
//    var warnPercentage = Obj.warninglevel / Obj.maxValue * 100;
//    var warnLevel = parseInt(warnPercentage);

//    var critPercentage = Obj.criticalLevel / Obj.maxValue * 100;
//    var critLevel = parseInt(critPercentage);


//    //  TODO change if the value is null (unknow state)
//    if (Obj.tag.value == null){

//       var parentElement = document.getElementById(Obj.parentID);

       

//    }

//    // Implement the Logic

//    var percentage = Obj.tag.value / Obj.maxValue * 100;

//    var actuelHeight = parseInt(percentage);

//    if (actuelHeight > warnLevel) {

//        newVProgress.className = "";
//        newVProgress.className = "vmeter orange";
//    }

//    if (actuelHeight > critLevel) {
//        newVProgress.className = "";

//        newVProgress.className = "vmeter red";
      

//    }

//    if (actuelHeight < warnLevel) {
//        newVProgress.className = "";

//        newVProgress.className = "vmeter";
//    }

//    var height = actuelHeight;

//    newVProgress.style.height = height + '%';;




//    newlabel.innerHTML = Obj.tag.value;


//}

//// ********************************************************************//
//// This will create the vertical progress bar . 
//// This user need to implement the Div ID in order to create the widget.
//// ********************************************************************//
//function HMIVertProgress(parentID) {

   

//    this.setPriority = function (value) {

//        this.priority = value;
//        if (this.tag.plc.AMSAddress.length > 0 && value.length > 0 && this.tag.type.length > 0
//            && this.priority !== undefined) {
//            this.bind();
//}

//}

//    this.HTMLVertProgressElement = createVerticalProgressBar2(parentID);

//    this.tag = new TagObject(new PLC(), "", "", this, evaluateVertProgress);

//    this.setMaxValue = function (value) {
//        this.maxValue = value;

//        if(!this.tag.value == null){
//            evaluateVertProgress(this);
//        }

//    }


//    this.setValueType =function(value){
//        this.tag.type = value
//        if (this.tag.plc.AMSAddress.length > 0 && this.tag.tagName.length > 0 && !value == undefined
//            && this.priority !== undefined) {
//            this.bind();
//        }         

//    }


//    this.setPLCAMSAddress = function (AMSAddress) {
//        this.tag.plc.AMSAddress = AMSAddress;
//        if (AMSAddress.length > 0 && this.tag.tagName.length > 0 && this.tag.type.length > 0
//            && this.priority !== undefined) {
//            this.bind();
//        }
//    }

//    this.setPLCRuntimePort = function (port) {
//        this.tag.plc.runtimePort = port;
//        if (this.tag.plc.AMSAddress.length > 0 && this.tag.tagName.length > 0 && this.tag.type.length > 0
//            && this.priority !== undefined) {
//            this.bind();
//        }
//    }

 

//     this.bind = function () {
//        if(this.priority === "medium") {
//            mediumPollingTagArray.push(this.tag);
//        }

//        if(this.priority === "fast") {
//            fastPollingTagArray.push(this.tag);
//        }

//        if (this.priority === "slow") {
//            slowPollingTagArray.push(this.tag);
//        }

//            //default polling
//        if (this.priority == undefined) {
//            mediumPollingTagArray.push(this.tag);

//            }
//            }


//    this.setMinValue = function (value) {
//        this.minValue = value;
//        if(!this.tag.value == null){
//            evaluateVertProgress(this);
//            }

//    }


//    this.setPLCtag = function (value) {


//        this.tag.tagName = value;

//        if (this.tag.plc.AMSAddress.length > 0 && value.length > 0  && this.tag.type.length > 0
//            && this.priority !== undefined) {
//            this.bind();
//        }


//        if(!this.tag.value == null){
//            evaluateVertProgress(this);
//        }

    
//    }



//    this.setwarninglevel = function (value) {
//        this.warninglevel = value;
//        if(!this.tag.value == null){
//            evaluateVertProgress(this);
//        }

//    }



//    this.setcriticalLevel = function (value) {
//        this.criticalLevel = value;

//        if(!this.tag.value == null){
//            evaluateVertProgress(this);
//        }

      
//    }

  
//    this.parentID = parentID;

//}




// ********************************************************************//
// ********************************************************************//
// ********************************************************************//

// ********************************************************************//
// 
// Toogle button
// ********************************************************************//
function createToggleWidget(PLCTag, htmlID) {

    var label = document.getElementById(htmlID);


    var newInput = document.createElement("input");

    newInput.type = "checkbox";

    newInput.classList.add('toggleswitch-input');

    label.appendChild(newInput);

    var newSpan1 = document.createElement('span');

    newSpan1.classList.add('toggleswitch-label');

    var newAtt1 = document.createAttribute("data-off");
    newAtt1.value = "No";

    var newAtt2 = document.createAttribute("data-on");
    newAtt2.value = "Yes";


    newSpan1.setAttributeNode(newAtt1);

    newSpan1.setAttributeNode(newAtt2);


    label.appendChild(newSpan1);

    var newSpan2 = document.createElement('span');

    newSpan2.classList.add('toggleswitch-handle');


    label.appendChild(newSpan2);


}


// ********************************************************************//
// ********************************************************************//
// ********************************************************************//
//  Top page notification bar logic

// ********************************************************************//
//  This function notify the HTML page that an error occured. 
//  Parameter : Mess is the message displayed in the notification bar.
//  Parameter :  Color is the type of warning.
//  TODO: Need to create generic way to set the error type. 
// 
// ********************************************************************//
function notifyBar(Mess, color) {

    bar = document.getElementById("note");

    var pos = 0;
    var id = setInterval(frame, 5);

    function frame() {
        if (pos == 5) {
            clearInterval(id);
        } else {
            pos++;
            bar.style.top = pos + 'px';
        }
    }

    bar.style.display = "block";

    bar.innerHTML = Mess
    bar.style.opacity = "0.8"

    //set the correct background color depending on notification.
    if (color == "Warning") {
        bar.style.background = "#e65c00"
    }
    else if (color == "Error") {
        bar.style.background = "#990000"
    }
    else if (color == "Notif") {
        bar.style.background = "#668cff"
    }
};

// ********************************************************************//
//  This function closes the top notfication bar. 
// ********************************************************************//
function closeNotifBar() {
    bar = document.getElementById("note");
    var pos = 0;
    var id = setInterval(frame, 5);

    function frame() {
        if (pos == -50) {
            clearInterval(id);
        } else {
            pos--;
            bar.style.top = pos + 'px';

        }
    }


}



