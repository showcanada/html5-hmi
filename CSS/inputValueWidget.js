﻿// ********************************************************************//
// This function create the numerical from the HMIobject 
// and parent div ID.
// ********************************************************************//
function createInputValue(parentID) {

    var parentDiv = document.getElementById(parentID);

    var val = document.createElement('input');

    val.id = parentID + "_Input";

 

    
    var inputForm = document.createElement('FORM');

    inputForm.id = parentID + "_form";   

    inputForm.appendChild(val);


    parentDiv.appendChild(inputForm);

    return parentDiv;
    
}



function InputNumericalWidget(parentID){

    this.parentID = parentID;

    this.originalValue;

    this.priority = "fast";

    this.HTMLInputNumericalHMTL = createInputValue(parentID);


    this.setLabelValue = function (value) {
        this.labelValue = value;

        var inputHTML = document.getElementById(this.parentID + "_Input");

        var labelDiv = document.createElement("div");

        labelDiv.id = "setLabel" + this.parentID;
        
        labelDiv.innerHTML = "";

        labelDiv.innerHTML =  this.labelValue;

        labelDiv.setAttribute("class", "inputLabel left");

        inputHTML.insertAdjacentElement('afterend', labelDiv);

    }


    this.setIsReadOnly = function (value) {
        this.isReadOnly = value;

        if (this.isReadOnly == 'true') {

            document.getElementById(this.parentID + "_Input").readOnly = true;

        }

        if (this.isReadOnly == 'false') {

            document.getElementById(this.parentID + "_Input").readOnly = false;

        }

    }

   
    this.bind = function () {
        if (this.priority === "medium") {
            mediumPollingTagArray.push(this.tag);
        }

        if (this.priority === "fast") {
            fastPollingTagArray.push(this.tag);
        }

        if (this.priority === "slow") {
            slowPollingTagArray.push(this.tag);
        }

        if (this.priority === "once") {
            oncePollingTagArray.push(this.tag);
        }

        //default polling
        if (this.priority == undefined) {
            oncePollingTagArray.push(this.tag);

        }
    }

    
    this.connectTag = function (plcObj, value, type) {
        this.tag = new TagObject(plcObj, "", type, this, getLatestValue);
        this.tag.tagName = value;
       

        if (this.tag.plc.AMSAddress.length > 0 && value.length > 0 && this.tag.type.length > 0
            && this.priority !== undefined) {
            this.bind();
        }


        if (!this.tag.value == null) {
            getLatestValue(this);
        }



    }

}


function getLatestValue(Obj, value) {

    var inputHTML = document.getElementById(Obj.parentID + "_Input");

    var inputform = document.getElementById(Obj.parentID + "_form");

    if (Obj.tag.type == 'string') {
        var str = Obj.tag.value;

        var length = str.indexOf('\0');    

        Obj.tag.value = str.slice(0, length);

        //console.log(inputHTML.value);
        //console.log(Obj.tag.value);

    }


    if (inputform.onsubmit == null) {
        inputform.onsubmit = function writeDataToPLC() {


            var tagObject = Obj.tag;

            tagObject.tagHandle = getVariableHandle(tagObject);

          
                       
          

            if (tagObject.tagHandle != 0) {
              
                if (tagObject.type == 'string') {
                    var str1 = inputHTML.value;

                    var length1 = inputHTML.value.length;
                    tagObject.value = inputHTML.value;

                    writeVariable(tagObject, length1+1);
                    inputHTML.value = tagObject.value;

                    console.log(tagObject.value);


                } else {

                    //TODO: Wrting the wrong string value.
                    tagObject.value = inputHTML.value;
                    writeVariable(tagObject);
                }

                                   
            }
            
        }
    }






    if (Obj.tag.value !== Obj.originalValue) {

         inputHTML.value = Obj.tag.value;

        Obj.originalValue = Obj.tag.value;
    }

}


