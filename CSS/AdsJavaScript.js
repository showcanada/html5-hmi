



var timeoutValue = 0;

const FAST_REFRESH_RATE = 100;

const MEDIUM_REFRESH_RATE = 1250;

const SLOW_REFRESH_RATE = 2500;

const ONCE_REFRESH_RATE = 100;




//==========================================================================================================================================
/*********************************************** ADS OBJECTS AND FUNCTIONS ****************************************************************/
//==========================================================================================================================================
    function PLC() {
        this.AMSAddress = "";
        this.runtimePort = 851;
        this.setAMSAddress = function (address) {
            this.AMSAddress = address + ".1.1";
            this.clientURL = "http://" + address + "/TcAdsWebService/TcAdsWebService.dll";
            this.client = new TcAdsWebService.Client(this.clientURL, null, null);
    }
	this.setRuntimePort = function (port) {
		this.runtimePort = port;
	}       
}

    function TagObject(plc, tagName, type, widgetObj, updateWidgetFunction) {
        this.plc = plc;
        this.tagName = tagName;
        this.type = type;
        this.widgetObj = widgetObj;
        this.tagHandle = null; //null = handle not requested yet, 0 = can't get handle.
        this.value = null;
        this.updateWidgetFunction = updateWidgetFunction;
}

    function ReadSumCommand(plc) {
        this.plc = plc;
        this.tagArray =[];
        this.addTag = function (tag) {
        this.tagArray.push(tag);
    }
    this.send = function () {
        var writer = new TcAdsWebService.DataWriter();
        var subCommandCount = this.tagArray.length;
        var totalReadSize = 0;
        //create all the sub commands:
        for (var i = 0; i < subCommandCount; i++) {
            writer.writeDINT(TcAdsWebService.TcAdsReservedIndexGroups.SymbolValueByHandle);//write the index group for a read with a handle.
            writer.writeDINT(this.tagArray[i].tagHandle); //write the index offset for the subcommand, which is the handle of the tag to read.
            writer.writeDINT(getSizeOfType(this.tagArray[i].type));//length of tag to read.
            totalReadSize += getSizeOfType(this.tagArray[i].type);
    }
        var response = this.plc.client.readwrite(this.plc.AMSAddress, this.plc.runtimePort, 0xF080, //the constant for the read sum command index group.
            subCommandCount, totalReadSize + subCommandCount * 4, // Length of requested data + length of error code (4 bytes per subcommand)
            writer.getBase64EncodedData(), //pointer to data to write, contains the read sub commands.
            null, //callback for the response, null because read operation is sync.
            null, //userstate == useless param
            timeoutValue, null, false);
        if (response.hasError) {
            //TODO handle error.
            //if an error occur, all tags' value are set to null (unknown).
            for (var i = 0; i < subCommandCount; i++) {
                this.tagArray[i].value = null;
        }
        } else {
            var reader = response.reader;
            for (var i = 0; i < subCommandCount; i++) {
                var subCommandError = reader.readDINT();
                if (subCommandError) {
                    //TODO handle error.
                    //if an error occur, the tag value is set to null (unknown).
                    this.tagArray[i].value = null;
                } else {
                    //if no error occur, the tag value is set to undefined (not read yet), to make a difference with 
                    //the values not to read (set to null after error in subcommand).
                    this.tagArray[i].value = undefined;
            }
        }
            for (var i = 0; i < subCommandCount; i++) {
                if (this.tagArray[i].value !== null)//if value is not null, it has been received (no error) and can be read.
                    this.tagArray[i].value = getValueFromReader(reader, this.tagArray[i].type);
        }
    }
    }
}

    function RequestHandlesSumCommand(plc) {
    this.plc = plc;
    this.tagArray =[];
    this.addTag = function (tag) {
        this.tagArray.push(tag);
    }
    this.send = function () {
        var writer = new TcAdsWebService.DataWriter();
        var subCommandCount = this.tagArray.length;
        for (var i = 0; i < subCommandCount; i++) {
            writer.writeDINT(TcAdsWebService.TcAdsReservedIndexGroups.SymbolHandleByName);//write the index group of the subcommand.
            writer.writeDINT(0); //write the index offset of the subcommand. always 0 for getting a handle.
            writer.writeDINT(4); //write the read length of the subcommand, 4 bytes for the handle.
            writer.writeDINT(this.tagArray[i].tagName.length); //write the write length of the subcommand (length of variable name);
    }
        //write all the tagname after the subcommands.
        for (var i = 0; i < subCommandCount; i++) {
            writer.writeString(this.tagArray[i].tagName);
    }
        var response = this.plc.client.readwrite(this.plc.AMSAddress, this.plc.runtimePort, 0xF082, subCommandCount, subCommandCount * 12, //4 bytes for the error code, 4 bytes for the length of the received data and 4 bytes for the handle. 
                        writer.getBase64EncodedData(), null, null, timeoutValue, null, false);
        if (response.hasError) {
            //TODO handle error.
            //if a global error occur, all tag are set to null.
            for (var i = 0; i < subCommandCount; i++) {
                this.tagArray[i].tagHandle = null;
        }
        } else {
            var reader = response.reader;
            for (var i = 0; i < subCommandCount; i++) {
                var errorCode = reader.readDINT();
                if (errorCode) {
                    //TODO handle error.
                    //if an error occur, the tag is set to 0, so it is not read when all handle are read at the end.
                    reader.readDINT();//skip the read length.
                    this.tagArray[i].tagHandle = 0;
                }
                else {
                    //write all the handle in the tagObject array
                    var length = reader.readDINT();//read the length of received data.
                    if (length == 4)
                        this.tagArray[i].tagHandle = null; //if length is correct, set the handle to null to know it has to be read at the end.
                    else {
                        this.tagArray[i].tagHandle = 0;//error if length is not 4 bytes, so the handle is set to 0.
                }
            }
        }//read all the handle (put at the end of the response.)
            for (var i = 0; i < subCommandCount; i++) {
                if (this.tagArray[i].tagHandle != 0)//if handle is present (no error)
                    this.tagArray[i].tagHandle = reader.readDINT();//read the handle.
        }
    }
    }
}

var fastPollingTagArray =[];
var mediumPollingTagArray =[];
var slowPollingTagArray =[];
var oncePollingTagArray =[];



(function () {

    window.setInterval(fastPolling, FAST_REFRESH_RATE);
    window.setInterval(mediumPolling, MEDIUM_REFRESH_RATE);
    window.setInterval(slowPolling, SLOW_REFRESH_RATE);
    window.setTimeout(oncePolling, ONCE_REFRESH_RATE);


}) ();

    function fastPolling() {
    if (fastPollingTagArray.length > 0) {
        var readSumCommandList =[];
        var requestHandleSumCommandList =[];
        for (var i = 0; i < fastPollingTagArray.length; i++) {
            var tag = fastPollingTagArray[i];
            //if sum commands exist, check if one exist for the plc of the current tag, else create new commands for this tag.
            var sumCommandsExist = false;
            for (var j = 0; j < readSumCommandList.length; j++) {
                if (readSumCommandList[j].plc === tag.plc) {
                    readSumCommandList[j].addTag(tag);
                    requestHandleSumCommandList[j].addTag(tag);
                    sumCommandsExist = true;
                    break;
            }
        }
            if (sumCommandsExist == false) {
                var readSumCommand = new ReadSumCommand(tag.plc);
                var requestHandleSumCommand = new RequestHandlesSumCommand(tag.plc);
                readSumCommand.addTag(tag);
                requestHandleSumCommand.addTag(tag);
                readSumCommandList.push(readSumCommand);
                requestHandleSumCommandList.push(requestHandleSumCommand);
        }
    }
        for (var i = 0; i < readSumCommandList.length; i++) {
            requestHandleSumCommandList[i].send();
            readSumCommandList[i].send();
    }
        updateWidgets(fastPollingTagArray);
    }
}


    function mediumPolling() {
    if (mediumPollingTagArray.length > 0) {
        var readSumCommandList =[];
        var requestHandleSumCommandList =[];
        for (var i = 0; i < mediumPollingTagArray.length; i++) {
            var tag = mediumPollingTagArray[i];
            //if sum commands exist, check if one exist for the plc of the current tag, else create new commands for this tag.
            var sumCommandsExist = false;
            for (var j = 0; j < readSumCommandList.length; j++) {
                if (readSumCommandList[j].plc === tag.plc) {
                    readSumCommandList[j].addTag(tag);
                    requestHandleSumCommandList[j].addTag(tag);
                    sumCommandsExist = true;
                    break;
            }
        }
            if (sumCommandsExist == false) {
                var readSumCommand = new ReadSumCommand(tag.plc);
                var requestHandleSumCommand = new RequestHandlesSumCommand(tag.plc);
                readSumCommand.addTag(tag);
                requestHandleSumCommand.addTag(tag);
                readSumCommandList.push(readSumCommand);
                requestHandleSumCommandList.push(requestHandleSumCommand);
        }
    }
        for (var i = 0; i < readSumCommandList.length; i++) {
            requestHandleSumCommandList[i].send();
            readSumCommandList[i].send();
    }
        updateWidgets(mediumPollingTagArray);
    }
}


    function slowPolling() {
    if (slowPollingTagArray.length > 0) {
        var readSumCommandList =[];
        var requestHandleSumCommandList =[];
        for (var i = 0; i < slowPollingTagArray.length; i++) {
            var tag = slowPollingTagArray[i];
            //if sum commands exist, check if one exist for the plc of the current tag, else create new commands for this tag.
            var sumCommandsExist = false;
            for (var j = 0; j < readSumCommandList.length; j++) {
                if (readSumCommandList[j].plc === tag.plc) {
                    readSumCommandList[j].addTag(tag);
                    requestHandleSumCommandList[j].addTag(tag);
                    sumCommandsExist = true;
                    break;
            }
        }
            if (sumCommandsExist == false) {
                var readSumCommand = new ReadSumCommand(tag.plc);
                var requestHandleSumCommand = new RequestHandlesSumCommand(tag.plc);
                readSumCommand.addTag(tag);
                requestHandleSumCommand.addTag(tag);
                readSumCommandList.push(readSumCommand);
                requestHandleSumCommandList.push(requestHandleSumCommand);
        }
    }
        for (var i = 0; i < readSumCommandList.length; i++) {
            requestHandleSumCommandList[i].send();
            readSumCommandList[i].send();
    }
        updateWidgets(slowPollingTagArray);
    }
}


    function oncePolling() {
    if (oncePollingTagArray.length > 0) {
        var readSumCommandList =[];
        var requestHandleSumCommandList =[];
        for (var i = 0; i < oncePollingTagArray.length; i++) {
            var tag = oncePollingTagArray[i];
            //if sum commands exist, check if one exist for the plc of the current tag, else create new commands for this tag.
            var sumCommandsExist = false;
            for (var j = 0; j < readSumCommandList.length; j++) {
                if (readSumCommandList[j].plc === tag.plc) {
                    readSumCommandList[j].addTag(tag);
                    requestHandleSumCommandList[j].addTag(tag);
                    sumCommandsExist = true;
                    break;
            }
        }
            if (sumCommandsExist == false) {
                var readSumCommand = new ReadSumCommand(tag.plc);
                var requestHandleSumCommand = new RequestHandlesSumCommand(tag.plc);
                readSumCommand.addTag(tag);
                requestHandleSumCommand.addTag(tag);
                readSumCommandList.push(readSumCommand);
                requestHandleSumCommandList.push(requestHandleSumCommand);
        }
    }
        for (var i = 0; i < readSumCommandList.length; i++) {
            requestHandleSumCommandList[i].send();
            readSumCommandList[i].send();
    }
        updateWidgets(oncePollingTagArray);
    }
}


    function updateWidgets(tagArray) {
        for (var i = 0; i < tagArray.length; i++) {
		tagArray[i].updateWidgetFunction(tagArray[i].widgetObj, tagArray[i].value);
    }
}

    function getVariableHandle(TagObj) {
        // Create writer for sending twincat symbol name;
        var handlewriter = new TcAdsWebService.DataWriter();
        // Write symbol name
        handlewriter.writeString(TagObj.tagName);
        var response = TagObj.plc.client.readwrite(
             TagObj.plc.AMSAddress, TagObj.plc.runtimePort, TcAdsWebService.TcAdsReservedIndexGroups.SymbolHandleByName, 	// IndexGroup = handle request
            0, // IndexOffset = 0 when requesting an handle
            4, // Length of requested data: 4 byte for the handle;
            handlewriter.getBase64EncodedData(),
            null,
            null,
            timeoutValue,
            null,
            false);
        if (response && !response.isBusy) {
		if (response.hasError) {
		    //TODO handle error.
		    return null;
		}
		else {
			var handle = response.reader.readDWORD();
			return handle;
            }
    }
	return 0;
}

    function getMultipleVariableHandles(tagArray) {
        var writer = new TcAdsWebService.DataWriter();
        var subCommandCount = tagArray.length;
        for (var i = 0; i < subCommandCount; i++) {
		writer.writeDINT(TcAdsWebService.TcAdsReservedIndexGroups.SymbolHandleByName);//write the index group of the subcommand.
		writer.writeDINT(0); //write the index offset of the subcommand. always 0 for getting a handle.
		writer.writeDINT(4); //write the read length of the subcommand, 4 bytes for the handle.
		writer.writeDINT(tagArray[i].tagName.length); //write the write length of the subcommand (length of variable name);
    }
        //write all the tagname after the subcommands.
        for (var i = 0; i < subCommandCount; i++) {
		writer.writeString(tagArray[i].tagName);
    }
	var response = client.readwrite(NETID, PORT, 0xF082, subCommandCount, subCommandCount * 12, //4 bytes for the error code, 4 bytes for the length of the received data and 4 bytes for the handle. 
					writer.getBase64EncodedData(), null, null, timeoutValue, null, false);
	if (response.hasError) {
					    //TODO handle error.
					    //if a global error occur, all tag are set to null.
		for (var i = 0; i < subCommandCount; i++) {
			tagArray[i].tagHandle = null;
					}
					} else {
		var reader = response.reader;
		for (var i = 0; i < subCommandCount; i++) {
			var errorCode = reader.readDINT();
			if (errorCode) {
			    //TODO handle error.
			    //if an error occur, the tag is set to 0, so it is not read when all handle are read at the end.
				reader.readDINT();//skip the read length.
				tagArray[i].tagHandle = 0;
			}
			else {
			    //write all the handle in the tagObject array
				var length = reader.readDINT();//read the length of received data.
				if (length == 4)
					tagArray[i].tagHandle = null; //if length is correct, set the handle to null to know it has to be read at the end.
				else {
					tagArray[i].tagHandle = 0;//error if length is not 4 bytes, so the handle is set to 0.
			}
		}
					}//read all the handle (put at the end of the response.)
		for (var i = 0; i < subCommandCount; i++) {
			if (tagArray[i].tagHandle != 0)//if handle is present (no error)
				tagArray[i].tagHandle = reader.readDINT();//read the handle.
					}
    }
}

    function readVariable(tagObject) {
        var response = tagObject.plc.client.read(tagObject.plc.AMSAddress, tagObject.plc.runtimePort, 0xF005, tagObject.tagHandle, getSizeOfType(tagObject.type), null, null, timeoutValue, null, false);
        if (response.hasError) {
            //TODO handle error.
		tagObject.value = null;
        }
        else {
		tagObject.value = getValueFromReader(response.reader, tagObject.type);
    }
}

    function writeVariable(tagObject, lenght) {
        var writer = new TcAdsWebService.DataWriter();
        writeValueToWriter(tagObject.value, tagObject.type, writer, lenght);
        var response = tagObject.plc.client.write(tagObject.plc.AMSAddress, tagObject.plc.runtimePort, 0xF005, tagObject.tagHandle, writer.getBase64EncodedData(), null, null, timeoutValue, null, false);
        if (response.hasError) {
            //TODO handle error.
    }
}

    function readMultipleVariables(tagArray) {
        var writer = new TcAdsWebService.DataWriter();
        var subCommandCount = tagArray.length;
        var totalReadSize = 0;
        //create all the sub commands:
        for (var i = 0; i < subCommandCount; i++) {
		writer.writeDINT(TcAdsWebService.TcAdsReservedIndexGroups.SymbolValueByHandle);//write the index group for a read with a handle.
		writer.writeDINT(tagArray[i].tagHandle); //write the index offset for the subcommand, which is the handle of the tag to read.
		writer.writeDINT(getSizeOfType(tagArray[i].type));//length of tag to read.
		totalReadSize += getSizeOfType(tagArray[i].type);
    }
	var response = client.readwrite(NETID, PORT, 0xF080, //the constant for the read sum command index group.
		subCommandCount, // IndexOffset = number of sub command.
		totalReadSize + subCommandCount * 4, // Length of requested data + length of error code (4 bytes per subcommand);
		writer.getBase64EncodedData(), //pointer to data to write, contains the read sub command.
		null, //callback for the response, null because read operation is sync.
		null, //userstate == useless param
		timeoutValue, null, false);
	if (response.hasError) {
		    //TODO handle error.
		    //if an error occur, all tags' value are set to null (unknown).
		for (var i = 0; i < subCommandCount; i++) {
			tagArray[i].value = null;
		}
		} else {


		var reader = response.reader;
		for (var i = 0; i < subCommandCount; i++) {
			var subCommandError = reader.readDINT();
			if (subCommandError) {
			    //TODO handle error.
			    //if an error occur, the tag value is set to null (unknown).
				tagArray[i].value = null;
			} else {
			    //if no error occur, the tag value is set to undefined (not read yet), to make a difference with 
			    //the values not to read (set to null after error in subcommand).
				tagArray[i].value = undefined;
		}
		}
		for (var i = 0; i < subCommandCount; i++) {
		    if (tagArray[i].value !== null)//if value is not null, it has been received (no error) and can be read.

		      
				tagArray[i].value = getValueFromReader(reader, tagArray[i].type);
		}
    }
}

    //==========================================================================================================================================
    /*************************************************** USEFUL FUNCTIONS *********************************************************************/
    //==========================================================================================================================================

    function getSizeOfType(type) {
    switch (type) {
        case "bool":
        case "byte":
            return 1;
        case "dint":
            return 4;
        case "word":
            return 4;
        case "dword":
            return 4;
        case "real":
            return 8;
        case "lreal":
            return 8;
        case "int":
            return 8;
        case "string":
            return 255;
        default:
            return 0;
    }
}

    function getValueFromReader(reader, valueType) {
        switch (valueType) {
		case "bool":
			return reader.readBOOL();
		case "byte":
		    return reader.readBYTE();
	    case "dint":
	        return reader.readDINT();
	    case "int":
	        return reader.readINT();
	    case "word":
	        return reader.readWORD();
	    case "dword":
	        return reader.readDWORD();
	    case "string":
	        return reader.readString(255);
	    case "real":
	        return reader.readREAL();
	    case "lreal":
	        return reader.readLREAL();
	    case "sint":
	        return reader.readSINT();

		default:
			return null;
    }
}

    function writeValueToWriter(value, type, writer ,lenght) {
        switch (type) {
		case "bool":
		    writer.writeBOOL(value); break;
	    case "dint":
	        writer.writeDINT(value); break;
	    case "byte":
	        writer.writeBYTE(value); break;
	    case "dword":
	        writer.writeDWORD(value); break;
	    case "word":
	        writer.writeWORD(value); break;
	    case "real":
	        writer.writeREAL(value); break;
	    case "lreal":
	        writer.writeLREAL(value); break;
	    case "int":
	        writer.writeINT(value); break;
	    case "sint":
	        writer.writeSINT(value); break;
        case "string":
	        writer.writeString(value ,lenght); break;

		default:
			break;
    }
}

