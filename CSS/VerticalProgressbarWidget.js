

// ********************************************************************//
// This function create the vertical progress bar from the HMIobject 
// and parent div ID.
// ********************************************************************//
function createVerticalProgressBar2(parentID) {

    // Create the actual display.
    //if (!Obj.type == typeof (HMIProgress)) {
    //    return;

    //}

    var parentDiv = document.getElementById(parentID);

    //
    var popupDiv = document.createElement('div');

    popupDiv.className = 'popup';

    var newspan = document.createElement('span');

    newspan.className = 'popuptext';

    newspan.id = parentID + '_VNewSpan';

    popupDiv.appendChild(newspan);



    parentDiv.appendChild(popupDiv);


    //
    var newVmeter = document.createElement('div');

    newVmeter.className = 'vmeter animate';

    var newVProgress = document.createElement('div');

    newVProgress.id = parentID + '_VProgress';

    var newlabel = document.createElement('Div');

    newlabel.className = 'vprogresslabel';

    newlabel.id = parentID + '_VProgressLabel';

    newVProgress.appendChild(newlabel);


    newVmeter.appendChild(newVProgress);


    parentDiv.appendChild(newVmeter);


}


// ********************************************************************//
// This function evaluate the plc tag value and evaluate if it reach
// warning, critical level and change the display in the HTML view.
//
// ********************************************************************//
 function evaluateVertProgress(Obj ,value) {

    var newVProgress = document.getElementById(Obj.parentID + '_VProgress');
    newVProgress.className = "vmeter";


    var newspan = document.getElementById(Obj.parentID + '_VNewSpan');

    var newlabel = document.getElementById(Obj.parentID + '_VProgressLabel');

    // Evaluation of treshold critical level
    var warnPercentage = Obj.warninglevel / Obj.maxValue * 100;
    var warnLevel = parseInt(warnPercentage);

    var critPercentage = Obj.criticalLevel / Obj.maxValue * 100;
    var critLevel = parseInt(critPercentage);


    //  TODO change if the value is null (unknow state)
    if (Obj.tag.value == null){

       var parentElement = document.getElementById(Obj.parentID);

       

    }

    // Implement the Logic

    var percentage = Obj.tag.value / Obj.maxValue * 100;

    var actuelHeight = parseInt(percentage);

    if (actuelHeight > warnLevel) {

        newVProgress.className = "";
        newVProgress.className = "vmeter orange";
    }

    if (actuelHeight > critLevel) {
        newVProgress.className = "";

        newVProgress.className = "vmeter red";
      

    }

    if (actuelHeight < warnLevel) {
        newVProgress.className = "";

        newVProgress.className = "vmeter";
    }

    var height = actuelHeight;

    newVProgress.style.height = height + '%';;




    newlabel.innerHTML = Obj.tag.value;


}

// ********************************************************************//
// This will create the vertical progress bar . 
// This user need to implement the Div ID in order to create the widget.
// ********************************************************************//
 function HMIVertProgress(parentID) {

   

    this.setPriority = function (value) {

        this.priority = value;
        if (this.tag.plc.AMSAddress.length > 0 && value.length > 0 && this.tag.type.length > 0
            && this.priority !== undefined) {
            this.bind();
}

    }


    this.connectTag = function (plcObj, value, type) {
        this.tag = new TagObject(plcObj, "", type, this, evaluateVertProgress);
        this.tag.tagName = value;

        if (this.tag.plc.AMSAddress.length > 0 && value.length > 0 && this.tag.type.length > 0
           && this.priority !== undefined) {
            this.bind();
        }

    }

    this.HTMLVertProgressElement = createVerticalProgressBar2(parentID);

   

    this.setMaxValue = function (value) {
        this.maxValue = value;

      

    }


     this.bind = function () {
        if(this.priority === "medium") {
            mediumPollingTagArray.push(this.tag);
        }

        if(this.priority === "fast") {
            fastPollingTagArray.push(this.tag);
        }

        if (this.priority === "slow") {
            slowPollingTagArray.push(this.tag);
        }

            //default polling
        if (this.priority == undefined) {
            mediumPollingTagArray.push(this.tag);

            }
            }


    this.setMinValue = function (value) {
        this.minValue = value;
      
    }






    this.setwarninglevel = function (value) {
        this.warninglevel = value;
    

    }



    this.setcriticalLevel = function (value) {
        this.criticalLevel = value;

       

      
    }

  
    this.parentID = parentID;

}

