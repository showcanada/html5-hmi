﻿function createConnectionWidget(parentID,Obj) {
    var bar = document.getElementById(parentID);

   return bar;
}


function ConnectionStatusWidget(parentID,plc) {

    this.parentId = parentID
    this.priority = "fast";


    this.PLCconnection = function (value) {
        this.plc = value;
        if (this.plc !== undefined) {
            this.pclname = new TagObject(this.plc, "", "string", this, evaluatePLCName);
            this.pclname.tagName = "MAIN.myPLCName";
            this.online = new TagObject(this.plc, "", "bool", this, getOnlineValue);
            this.online.tagName = "MAIN.IsConnected";
            this.bind();
         
        }

    }

    this.bind = function () {
        if (this.priority === "medium") {
        
            mediumPollingTagArray.push(this.online);
            mediumPollingTagArray.push(this.pclname);
        }

        if (this.priority === "fast") {
          
            fastPollingTagArray.push(this.online);
            fastPollingTagArray.push(this.pclname);
        }

        if (this.priority === "slow") {
         
            slowPollingTagArray.push(this.online);
            slowPollingTagArray.push(this.pclname);

        }

        if (this.priority === "once") {
            
            oncePollingTagArray.push(this.online);
            oncePollingTagArray.push(this.pclname);
        }

        //default polling
        if (this.priority == undefined) {
         
            oncePollingTagArray.push(this.online);
            oncePollingTagArray.push(this.pclname);
        }
    }


    this.HMTLConnectionStatus = createConnectionWidget(this.parentId, this);

}



function getOnlineValue(Obj,value) {
    bar = document.getElementById("connectionStatus");

}


function evaluatePLCName(Obj, value) {


    bar = document.getElementById("connectionStatus");
    var isOnline = " is online."
    var isNotOnline =" is not online."

    if (Obj.pclname.value == 'null') {
    
        bar.innerHTML = Obj.pclname.value + isNotOnline;

    }


    if (Obj.pclname.value !== 'unknown' && Obj.online.value !== 'unknown') {
        bar.innerHTML = "";

        var str = Obj.pclname.value;

        var length = str.indexOf('\0');

        str = str.slice(0, length);

        bar.innerHTML = str + isOnline;

    }

 }


    function Connection(Mess, color) {

        bar = document.getElementById("connectionStatus");


        var pos = 0;
        var id = setInterval(frame, 5);

        function frame() {
            if (pos == 5) {
                clearInterval(id);
            } else {
                pos++;
                bar.style.top = pos + 'px';
            }
        }

        bar.style.display = "block";

        bar.innerHTML = "";
        bar.style.opacity = "0.8"

        //set the correct background color depending on notification.
        if (color == "Warning") {
            bar.style.background = "#e65c00"
        }
        else if (color == "Error") {
            bar.style.background = "#990000"
        }
        else if (color == "Notif") {
        
        }
        else {
            bar.style.background = "#668cff"
        }
    };