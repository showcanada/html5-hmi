﻿//********************************************************************************************************//
//                                                                                                        //                                                                                                       
//                                                                                                        // 
//   The momentary button has the following assumptions.                                                  //                        
//          -- All the PLC tag are boolean                                                                // 
//                  -- It is the PLC that would do the logic of creating an action following a            //  
//                     boolean value changes.                                                             // 
//                  -- All PLC value should be initiated at FALSE value and TRUE value is an              // 
//                     PCL action trigger.                                                                // 
//                                                                                                        // 
//                                                                                                        // 
//           -- All color skins should have the red color when the momentary button action is initiated.  // 
//                                                                                                        // 
//           -- All color skins should have the grey color when the momentary button statuss is unknown.  // 
//                                                                                                        // 
//                                                                                                        //  
//                                                                                                        // 
//   TODO: Missing function SetSize, SetColorSkins, SetColors(MainColor, HoverColor, ActiveColor)         // 
//                                                                                                        // 
//********************************************************************************************************// 




function MomentaryButton(parentID) {

    this.parentId = parentID;


    this.priority = "fast";

  this.requestListObj = [];

    this.requestTags =[];

    this.requestTags.contains = function (needle) {
        for (i in this) {
            if (this[i]== needle) return true;
        }
        return false;
    }


      this.addRequesTag = function (plcObj, value) {

          var objTag = new TagObject(plcObj, "", "bool", this, evaluateRequestValue);

        objTag.tagName = value;


        if (!this.requestTags.contains(objTag.tagName)) {
            this.requestTags.push(objTag.tagName);

            this.requestListObj.push(objTag);

            this.bindRequestTags();

            }
            
            }


    this.bindRequestTags = function () {
        this.requestListObj.forEach(function (requestTagObj, index) {


            fastPollingTagArray.push(requestTagObj);

            })

            };



    this.connectFeedbackTag = function (plcObj, value) {
        this.feedbackTag = new TagObject(plcObj, "", "bool", this, evaluateFeedBackValue);

        this.feedbackTag.tagName = value;


        if (this.feedbackTag.plc.AMSAddress.length > 0 && this.feedbackTag.tagName.length > 0
           && this.priority !== undefined) {
            this.bind();
            }
            }




    this.setTextValue = function (text) {
        var button = document.getElementById(this.parentId + "_bnt");

        button.value = text;
    }

   
 

    this.bind = function () {
        if (this.priority === "medium") {
            mediumPollingTagArray.push(this.feedbackTag);

        }

        if (this.priority === "fast") {
            fastPollingTagArray.push(this.feedbackTag);

        }

        if (this.priority === "slow") {
            slowPollingTagArray.push(this.feedbackTag);

        }

        //default polling
        if (this.priority == undefined) {
            mediumPollingTagArray.push(this.feedbackTag);
            
        }

     }

     
  

    this.setLabelPosition = function (value) {
 
        var button = document.getElementById(this.parentId + "_bnt");

        var label = document.getElementById(this.parentId + '_lbl');

        var temptext;

        if (button.value == "") {
            console.log("You must create the label for " + this.parentId + ' before setting the momentary position.');
        }
        temptext = button.value;
        button.value = "";


        label.innerHTML = temptext;
            //set the position;

        if (value == "top") {

            label.setAttribute('class', 'momentaryLabel top');

        }

       if (value == "botom") {
           label.setAttribute('class', 'momentaryLabel botom');

        }

      if (value == "left") {
           label.setAttribute('class', 'momentaryLabel left');

        }

      if (value == "right") {
          label.setAttribute('class', 'momentaryLabel right');

        }
        
    }


    this.HTMLMomentaryButton = createMomentaryButton(this.parentId, this);
    
    this.HTMLMomentaryButton.requestListObj = this.requestListObj;

    this.HTMLMomentaryButton.feedbackTag = this.feedbackTag;

    this.HTMLMomentaryButton.parentID = this.parentId;



}


function createMomentaryButton(parentID , Obj) {

    var button = document.createElement('input');   

    var buttonClass = button.classList;

    button.type = "submit";

    button.id = parentID + "_bnt";
    button.value = "";

    button.addEventListener("mousedown", onButtonPress);
    button.addEventListener("mouseup", onButtonRelease);

    buttonClass.add('momentaryButton');

    var labelElement = document.createElement("Div");
    labelElement.id = parentID + '_lbl';
      

    document.getElementById(parentID).appendChild(button);
    document.getElementById(parentID).appendChild(labelElement);

    
    return button;

}

var mousedownID = -1;
var output = 0;

function onButtonPress() {

    console.log("you have PRESSED the button");   


    //this.requestTag.tagHandle = getVariableHandle(this.requestTag);


    var buttonElement = document.getElementById(this.parentID + "_bnt");

     if(mousedownID==-1) { //Prevent multimple loops!
         mousedownID = setInterval(whilemousedown, 100, this /*execute every 100ms*/);
     }

}


function whilemousedown(tag) {
    
 tag.requestListObj.forEach(function (reqTag, index) {
        reqTag.tagHandle = getVariableHandle(reqTag);
        if (reqTag.tagHandle != 0) {

            if(reqTag.value == 0) {

                reqTag.value = 1;

                writeVariable(reqTag);

                }

                }
                })


}


function onButtonRelease() {

    console.log("you have RELEASED the button");

    output = 0;

    if(mousedownID != - 1) {  //Only stop if exists
     clearInterval(mousedownID);
     mousedownID=-1;
    }
    
    this.requestListObj.forEach(function (reqTag, index) {
        reqTag.tagHandle = getVariableHandle(reqTag);
        if (reqTag.tagHandle != 0) {

            if(reqTag.value == 1) {

                reqTag.value = 0;

                writeVariable(reqTag);

     }

     }
     })

}


function evaluateRequestValue(Obj, value) {


    var button = document.getElementById(Obj.parentId + "_bnt");


    var requestVals =[];

        requestVals.contains = function (needle) {
            for(i in this) {
                if (this[i]== needle) return true;
                }
            return false;
                }

    requestVals.allValuesSame = function () {

            for (var i = 1; i < this.length; i++) {
                if (this[i]!== this[0])
                    return false;
                    }

            return true;
}

   Obj.requestListObj.forEach(function (reqTag, index) {
        requestVals.push(reqTag.value);
})




    if (requestVals.contains('0')) {
        
        if (!button.classList.contains('momentaryButton')) {

            if (button.classList.contains('momentaryButton')) {

                button.classList.remove('momentaryButton');

            }

            if (button.classList.contains('momentaryButton_request')) {

                button.classList.remove('momentaryButton_request');

            }

            if (button.classList.contains('momentaryButton_unknown')) {

                button.classList.remove('momentaryButton_unknown');

            }

            if (button.classList.contains('momentaryButton_active')) {

                button.classList.remove('momentaryButton_active');

            }


            button.classList.add('momentaryButton');

        }

        
     }

    if ((requestVals.contains('1')) && (requestVals.allValuesSame())) {

        if (!button.classList.contains('momentaryButton_request')) {

            if (button.classList.contains('momentaryButton')) {

                button.classList.remove('momentaryButton');

            }

            if (button.classList.contains('momentaryButton_unknown')) {

                button.classList.remove('momentaryButton_unknown');

            }

            button.classList.add('momentaryButton_request');

        }

    }

    if (requestVals.contains('null')) {

        if (button.classList.contains('momentaryButton')) {

            button.classList.remove('momentaryButton');
            button.classList.add('momentaryButton_unknown');

        }

        if (button.classList.contains('momentaryButton_request')) {

            button.classList.remove('momentaryButton_request');
        button.classList.add('momentaryButton_unknown');

        }

    }


}


function evaluateFeedBackValue(Obj, value) {
    var button = document.getElementById(Obj.parentId + "_bnt");

      var requestVals =[];

        requestVals.contains = function (needle) {
            for(i in this) {
                if (this[i]== needle) return true;
             }
        return false;
             }

    requestVals.allValuesSame = function () {

        for (var i = 1; i < this.length; i++) {
            if (this[i]!== this[0])
                    return false;
                    }

            return true;
    }

 Obj.requestListObj.forEach(function (reqTag, index) {
        requestVals.push(reqTag.value);
        })




    if ((!(requestVals.contains('0'))) && (Obj.feedbackTag.value == 1)) {

        if (button.classList.contains('momentaryButton_request')) {

            button.classList.remove('momentaryButton_request');
        }

        if (button.classList.contains('momentaryButton_unknown')) {

            button.classList.remove('momentaryButton_unknown');
        }

        if (button.classList.contains('momentaryButton_active')) {

            button.classList.remove('momentaryButton_active');
        }

        if (button.classList.contains('momentaryButton')) {

            button.classList.remove('momentaryButton');
        }

        button.classList.add('momentaryButton_active');
    }

    if ((!(requestVals.contains('1'))) && (Obj.feedbackTag.value == 0)) {

        if (button.classList.contains('momentaryButton_request')) {

            button.classList.remove('momentaryButton_request');

        }

        if (button.classList.contains('momentaryButton_unknown')) {

            button.classList.remove('momentaryButton_unknown');

        }

        if (button.classList.contains('momentaryButton_active')) {

            button.classList.remove('momentaryButton_active');

        }

        if (button.classList.contains('momentaryButton')) {

            button.classList.remove('momentaryButton');

        }

        button.classList.add('momentaryButton_request');


    }

    if ((requestVals.contains('0'))) {

        if (button.classList.contains('momentaryButton_request')) {

            button.classList.remove('momentaryButton_request');

        }

        if (button.classList.contains('momentaryButton_unknown')) {

            button.classList.remove('momentaryButton_unknown');

        }

        if (button.classList.contains('momentaryButton_active')) {

            button.classList.remove('momentaryButton_active');

        }

        if (button.classList.contains('momentaryButton')) {

            button.classList.remove('momentaryButton');

        }

        button.classList.add('momentaryButton');

    }

}