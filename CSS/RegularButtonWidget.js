﻿//********************************************************************************************************//
//                                                                                                        //                                                                                                       
//                                                                                                        // 
//   The relugar button has the following assumptions.                                                    //                        
//          -- All the PLC tag are boolean                                                                // 
//                  -- It is the PLC that would do the logic of creating an action following a            //  
//                     boolean value changes.                                                             // 
//                  -- All PLC value should be initiated at FALSE value and TRUE value is an              // 
//                     PCL action trigger.                                                                // 
//                                                                                                        // 
//                                                                                                        // 
//           -- All color skins should have the red color when the regular button action is initiated.    // 
//                                                                                                        // 
//           -- All color skins should have the grey color when the regular button statuss is unknown.    // 
//                                                                                                        // 
//                                                                                                        //  
//                                                                                                        // 
//   TODO: Missing function SetSize, SetColorSkins, SetColors(MainColor, HoverColor, ActiveColor)         // 
//                                                                                                        // 
//********************************************************************************************************// 





//Call to create button.
function createwidgetButton(parentId ,Obj) {

    var button = document.createElement('input');

    var buttonClass = button.classList;

    button.type = "submit";

    button.id = parentId + "_bnt";
    button.value = "";

    button.addEventListener("click", function () {
        onButtonClick(Obj);
    }, false);

   
    buttonClass.add('regularButton');

    var labelElement = document.createElement("Div");
    labelElement.id = parentId + '_lbl';


    document.getElementById(parentId).appendChild(button);
    document.getElementById(parentId).appendChild(labelElement);

    //Obj.feedbackTag.value.onchange = evaluateFeedbackTagValueChanged(Obj);


    return button;


}


function onButtonClick(Obj) {

    Obj.requestListObj.forEach(function (reqTag, index) {
        reqTag.tagHandle = getVariableHandle(reqTag);
        if (reqTag.tagHandle != 0) {

            if (reqTag.value == 0) {

                reqTag.value = 1;

                writeVariable(reqTag);

            } else if (reqTag.value == 1) {

                reqTag.value = 0;

                writeVariable(reqTag);

            }

        }
      })
       
}


function regualButton(parentID) {

      
    this.requestTags = [];

    this.requestTags.contains = function (needle) {
        for (i in this) {
            if (this[i] == needle) return true;
        }
        return false;
    }

    this.requestListObj = [];

    this.requestListObj.contains = function (needle) {
        for (i in this) {
            if (this[i] == needle) return true;
        }
        return false;
    }

    this.addRequesTag = function (plcObj, value) {
       

        var objTag = new TagObject(plcObj, "", "bool", this, evaluateRequestedTagValue);

        objTag.tagName = value;


        if (!this.requestTags.contains(objTag.tagName)) {
            this.requestTags.push(objTag.tagName);

            this.requestListObj.push(objTag);

            this.bindRequestTags();

    }
      


    } 


    this.bindRequestTags = function () {
        this.requestListObj.forEach(function (requestTagObj, index) {
            

            fastPollingTagArray.push(requestTagObj);

        })

    };


    this.parentId = parentID;

    this.priority = "fast";

    this.setTextValue = function (text) {
            var button = document.getElementById(this.parentId + "_bnt");

         button.value = text;
    }


    this.connectFeedbackTag = function (plcObj, value) {
        this.feedbackTag = new TagObject(plcObj, "", "bool", this, evaluateFeedbackTagValue);

        this.feedbackTag.tagName = value;


        if (this.feedbackTag.plc.AMSAddress.length > 0 && this.feedbackTag.tagName.length > 0
           && this.priority !== undefined) {
            this.bind();
            }
    }

          

    this.bind = function () {
        if (this.priority === "medium") {
            mediumPollingTagArray.push(this.feedbackTag);
             }

        if (this.priority === "fast") {
            fastPollingTagArray.push(this.feedbackTag);
            }

        if (this.priority === "slow") {
            slowPollingTagArray.push(this.feedbackTag);
            }

        //default polling
        if (this.priority == undefined) {
            mediumPollingTagArray.push(this.feedbackTag);
            }
         }

  
     


this.setLabelPosition = function (value) {

        var button = document.getElementById(this.parentId + "_bnt");

        var label = document.getElementById(this.parentId + '_lbl');

    var temptext;

        if (button.value == "") {
            console.log("You must create the label for " + this.parentId + ' before setting the regular position');
}
    temptext = button.value;
    button.value = "";



        label.innerHTML = temptext;
    //set the position;

        if (value == "top") {

            label.setAttribute('class', 'regularLabel top');

}

        if (value == "botom") {
            label.setAttribute('class', 'regularLabel botom');

}

        if (value == "left") {
            label.setAttribute('class', 'regularLabel left');

}

        if (value == "right") {
            label.setAttribute('class', 'regularLabel right');

}




    }

    this.HTMLRegularButton = createwidgetButton(this.parentId, this);

    this.HTMLRegularButton.requestTag = this.requestTag;
    this.HTMLRegularButton.parentID = this.parentId;

}


function evaluateFeedbackTagValue(Obj, value) {
    var button = document.getElementById(Obj.parentId + "_bnt");

    var requestVals = [];

    requestVals.contains = function (needle) {
        for(i in this) {
            if(this[i]== needle) return true;
            }
        return false;
    }


    Obj.requestListObj.forEach(function (reqTag, index) {
        requestVals.push(reqTag.value);
    })
 
    // change evaluation 

    if (Obj.feedbackTag.value == 1 && (!(requestVals.contains('0')))) {

      

        if(button.classList.contains('regularButton_requested')) {

            button.classList.remove('regularButton_requested');

        }

        if (button.classList.contains('regularButton_unknown')) {

            button.classList.remove('regularButton_unknown');

        }

        if (button.classList.contains('regularButton_active')) {

            button.classList.remove('regularButton_active');

        }


        if (button.classList.contains('regularButton')) {

            button.classList.remove('regularButton');

        }

        button.classList.add('regularButton_active');



    }

// change evaluation 
    if (Obj.feedbackTag.value == 0 && (!(requestVals.contains('1')))) {

        if (button.classList.contains('regularButton_requested')) {

            button.classList.remove('regularButton_requested');

        }

        if (button.classList.contains('regularButton_unknown')) {

            button.classList.remove('regularButton_unknown');

        }

        if (button.classList.contains('regularButton_active')) {

            button.classList.remove('regularButton_active');

        }

        if (button.classList.contains('regularButton')) {

            button.classList.remove('regularButton');

        }


        button.classList.add('regularButton');
    }


    // change evaluation 
    if (Obj.feedbackTag.value == 0 && (!(requestVals.contains('0')))) {

        if (button.classList.contains('regularButton_requested')) {

            button.classList.remove('regularButton_requested');

        }

        if (button.classList.contains('regularButton_unknown')) {

            button.classList.remove('regularButton_unknown');

        }

        if (button.classList.contains('regularButton_active')) {

            button.classList.remove('regularButton_active');

        }

        if (button.classList.contains('regularButton')) {

            button.classList.remove('regularButton');

        }


        button.classList.add('regularButton_requested');
    }


}


function evaluateRequestedTagValue(Obj, value) {

    var button = document.getElementById(Obj.parentId + "_bnt");

    var requestVals = [];

    requestVals.contains = function (needle) {
        for (i in this) {
            if (this[i] == needle) return true;
        }
        return false;
    }

    requestVals.allValuesSame = function () {

        for (var i = 1; i < this.length; i++) {
            if (this[i] !== this[0])
                return false;
        }

        return true;
    }


    Obj.requestListObj.forEach(function (reqTag, index) {
        requestVals.push(reqTag.value);
    })

    // change evaluation 

    if (requestVals.contains('0')){

        if (!button.classList.contains('regularButton')) {

            if (button.classList.contains('regularButton_requested')) {

                button.classList.remove('regularButton_requested');

            }

            if (button.classList.contains('regularButton_unknown')) {

                button.classList.remove('regularButton_unknown');
             }

            if (button.classList.contains('regularButton_active')) {

                button.classList.remove('regularButton_active');

            }

            
            button.classList.add('regularButton');

        }

      }


    // change evaluation 

    if ((requestVals.contains('1')) && (requestVals.allValuesSame()) ) {
        if (button.classList.contains('regularButton_active')) {
            
            return;
        }


        if (!button.classList.contains('regularButton_requested')) {

            if (button.classList.contains('regularButton')) {

                button.classList.remove('regularButton');

            }

            if (button.classList.contains('regularButton_unknown')) {

                button.classList.remove('regularButton_unknown');

            }




            button.classList.add('regularButton_requested');

        }


    }

    // change evaluation 

    if (requestVals.contains('null')) {

        if (button.classList.contains('regularButton')) {

            button.classList.remove('regularButton');
            button.classList.add('regularButton_unknown');

        }

        if (button.classList.contains('regularButton_requested')) {

            button.classList.remove('regularButton_requested');
            button.classList.add('regularButton_unknown');

        }

    }


}