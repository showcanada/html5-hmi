﻿






function ToogleButton(parentID) {

    this.parentId = parentID

    this.priority = "fast";

    this.HMIToggleButtonElement = createToogleButton(parentID, this);

   


    this.setBooleanLabelValue = function (tValue, fValue) {
        var tLabelValue = tValue;
        var fLabelValue = fValue;

        var labelSpan = document.getElementById(this.parentId + '_lblSpan');
        labelSpan.setAttribute('data-on', tLabelValue);

        labelSpan.setAttribute('data-off', fLabelValue);

    };


    this.connectTag = function (plcObj, value) {
        this.tag = new TagObject(plcObj, "", "bool", this, updateToggleButton);

        this.tag.tagName = value;

        if (this.tag.plc.AMSAddress.length > 0 && this.tag.tagName.length > 0
           && this.priority !== undefined) {
            this.bind();
        }
    }

    this.bind = function () {
        if (this.priority === "medium") {
            mediumPollingTagArray.push(this.tag);
        }

        if (this.priority === "fast") {
            fastPollingTagArray.push(this.tag);
        }

        if (this.priority === "slow") {
            slowPollingTagArray.push(this.tag);
        }

        //default polling
        if (this.priority == undefined) {
            mediumPollingTagArray.push(this.tag);

        }

    }

      
}



function createToogleButton(parentID, Obj) {

    var parentDiv = document.getElementById(parentID);

    var parentLabel = document.createElement("LABEL");

    parentLabel.id = parentID + '_label';

    parentLabel.setAttribute('class', 'toggleswitch');

    var inputChkbox = document.createElement("input");
    inputChkbox.type = "checkbox";
    inputChkbox.id = parentID + '_cbox';
    inputChkbox.setAttribute('class', 'toggleswitch-input');



    var labelspan = document.createElement("span");
    labelspan.id = parentID + '_lblSpan';
    labelspan.setAttribute('class', 'toggleswitch-label');


    labelspan.setAttribute('data-on', 'on');
    labelspan.setAttribute('data-off', 'off');


    var handlespan = document.createElement("span");

    handlespan.id = parentID + '_hdlSpan';

    handlespan.setAttribute('class', 'toggleswitch-handle');


    parentLabel.addEventListener("click", function () { changeCheckBox(Obj); });



    parentLabel.appendChild(inputChkbox);
    parentLabel.appendChild(labelspan);
    parentLabel.appendChild(handlespan);

    

    parentDiv.appendChild(parentLabel);
  

    
}



function updateToggleButton(Obj, value) {

    var checkboxSpan = document.getElementById(Obj.parentId + '_cbox');


    if(value == true){
    
        checkboxSpan.checked = value;
    }

    if (value == false) {
        checkboxSpan.checked = value;    
    }

}


function changeCheckBox(Obj) {

    var checkboxSpan = document.getElementById(Obj.parentId + '_cbox');

    Obj.tag.tagHandle = getVariableHandle(Obj.tag);




    if (checkboxSpan.checked) {
        checkboxSpan.checked = false;

        if (Obj.tag.tagHandle != 0) {
            if (Obj.tag.value == 1) {
                Obj.tag.value = 0;
                writeVariable(Obj.tag);

            }
        }

    }
    else{
        checkboxSpan.checked = true;

        if (Obj.tag.tagHandle != 0) {
            if (Obj.tag.value == 0) {
                Obj.tag.value = 1;
                writeVariable(Obj.tag);

            }
        }
    
    }







 

    }