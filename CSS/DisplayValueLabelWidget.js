﻿
// ********************************************************************//
// ********************************************************************//
// ********************************************************************//
// Numerical Value


function DisplayValueLabel(parentID) {


    this.DisplayValueLabelElement = createDisplayLabelElement(parentID);


    this.connectTag = function (plcObj, value, type) {
        this.tag = new TagObject(plcObj, "", type, this, evaluateDisplayLabelValue);
        this.tag.tagName = value;
        this.tag.type = type;


        if (this.tag.plc.AMSAddress.length > 0
            && value.length > 0 && this.tag.type.length > 0
            && this.priority !== undefined) {
            this.bind();
        }


        if (!this.tag.value == null) {
            evaluateNumericalValue(this);
        }

        
    }
    
    this.setPriority = function (value) {
        this.priority = value;
        if (this.tag.plc.AMSAddress.length > 0
            && this.tag.tagName.length > 0
            && this.tag.type.length > 0
            && this.priority !== undefined) {
            this.bind();
        }
    }

    
    this.setUnitMesurement = function (value) {

        this.unitMesurement = value;

    }
    

    this.bind = function () {
        if (this.priority === "medium") {
            mediumPollingTagArray.push(this.tag);
        }

        if (this.priority === "fast") {
            fastPollingTagArray.push(this.tag);
        }

        if (this.priority === "slow") {
            slowPollingTagArray.push(this.tag);
        }

        //default polling
        if (this.priority == undefined) {
            mediumPollingTagArray.push(this.tag);

        }
    }



    this.parentID = parentID;


}


// ********************************************************************//
// This function create the numerical from the HMIobject 
// and parent div ID.
// ********************************************************************//
function createDisplayLabelElement(parentID) {

    var parentDiv = document.getElementById(parentID);

    var val = document.createElement('LABEL');

    val.id = parentID + "_value";

    var unit = document.createElement('LABEL');

    unit.id = parentID + "_units";


    parentDiv.appendChild(val);

    parentDiv.appendChild(unit);

}


// ********************************************************************//
// This function evaluate the plc tag value and evaluate if it reach
// warning, critical level and change the display in the HTML view.
//
// ********************************************************************//
function evaluateDisplayLabelValue(Obj, value) {

    var valueDiv = document.getElementById(Obj.parentID + "_value");

    var unitDiv = document.getElementById(Obj.parentID + "_units");

    if (Obj.tag.type == 'string') {
        var str = Obj.tag.value;

        var length = str.indexOf('\0');

        valueDiv.innerHTML = str.slice(0, length);
      
    } else {
        valueDiv.innerHTML = Obj.tag.value;
        
    }

    if (Obj.unitMesurement !== undefined) {
    
        unitDiv.innerHTML = " " + Obj.unitMesurement;

    }



  




}